<?php

namespace tests\models;

use app\models\User;
use Codeception\Test\Unit;

/**
 * Class UserTest
 * @package tests\models
 */
class UserTest extends Unit
{
    /**
     * @throws \Exception
     */
    public function testGetAge()
    {
        /**
         * Generate an age between 1 and legal age minus one
         */
        $age = rand(1, (User::legalAge - 1));
        $user = new User();
        $user->personal_code = $this->generatePersonalCode($age);
        self::assertEquals($age, $user->age);

        /**
         * Generate an age between legal age and 40
         */
        $age = rand(User::legalAge, 40);
        $user = new User();
        $user->personal_code = $this->generatePersonalCode($age);
        self::assertEquals($age, $user->age);
    }

    /**
     * @throws \Exception
     */
    public function testIsUnderage()
    {
        /**
         * Generate an age between 1 and legal age minus one
         */
        $age = rand(1, (User::legalAge - 1));
        $user = new User();
        $user->personal_code = $this->generatePersonalCode($age);
        self::assertEquals(true, $user->isUnderage());

        /**
         * Generate an age between legal age and 40
         */
        $age = rand(User::legalAge, 40);
        $user = new User();
        $user->personal_code = $this->generatePersonalCode($age);
        self::assertEquals(false, $user->isUnderage());
    }

    /**
     * Generate personal code for a person with given age
     *
     * @param $age
     * @return string
     * @throws \Exception
     */
    private function generatePersonalCode($age)
    {
        $now = new \DateTime('now');
        /**
         * Get dateOfBirth based on generated age
         */
        $dateOfBirth = $now->modify('-' .  $age . ' year')->format('Y-m-d');
        $century = $dateOfBirth[0] == '2' ? '5' : '4';
        $year = substr($dateOfBirth, 2, 2);
        $month = substr($dateOfBirth, 5, 2);
        $day = substr($dateOfBirth, 8, 2);

        return $century . $year . $month . $day . '1234';
    }
}
