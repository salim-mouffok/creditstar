<?php
namespace tests\models;

use app\models\AppUser;

class AppUserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = AppUser::findIdentity(100));
        expect($user->username)->equals('admin');

        expect_not(AppUser::findIdentity(999));
    }

    public function testFindUserByAccessToken()
    {
        expect_that($user = AppUser::findIdentityByAccessToken('100-token'));
        expect($user->username)->equals('admin');

        expect_not(AppUser::findIdentityByAccessToken('non-existing'));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = AppUser::findByUsername('admin'));
        expect_not(AppUser::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = AppUser::findByUsername('admin');
        expect_that($user->validateAuthKey('test100key'));
        expect_not($user->validateAuthKey('test102key'));

        expect_that($user->validatePassword('admin'));
        expect_not($user->validatePassword('123456'));        
    }

}
