<?php

namespace tests\services;

use app\services\UserService;
use Codeception\Test\Unit;

/**
 * Class UserServiceTest
 * @package tests\services
 */
class UserServiceTest extends Unit
{
    /**
     * @var array $personalCodes Personal codes to test from
     */
    protected $personalCodes = [
        '29207066894' => [
            'dateOfBirth' => '1892/07/06',
            'age' => ''
        ],
        '19708226894' => [
            'dateOfBirth' => '1897/08/22',
            'age' => ''
        ],
        '35103207895' => [
            'dateOfBirth' => '1951/03/20',
            'age' => ''
        ],
        '49108154782' => [
            'dateOfBirth' => '1991/08/15',
            'age' => ''
        ],
        '50301304578' => [
            'dateOfBirth' => '2003/01/30',
            'age' => ''
        ],
    ];

    /**
     * @throws \Exception
     */
    public function _before()
    {
        parent::_before();
        $this->prepareCurrentAges();
    }

    /**
     * @throws \yii\base\Exception
     */
    public function testGetAge()
    {
        foreach ($this->personalCodes as $code => $data) {
            self::assertEquals($data['age'], UserService::getAge($code));
        }
    }

    /**
     * Calculate current ages based on dates of birth
     *
     * @throws \Exception
     */
    private function prepareCurrentAges()
    {
        foreach ($this->personalCodes as $code => &$data) {
            $data['age'] = \DateTime::createFromFormat('Y/m/d', $data['dateOfBirth'])
                ->diff(new \DateTime('now'))
                ->y;
        }
    }
}
