<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * Class Loan
 *
 * @package app\models
 * @property int $id
 * @property int $user_id
 * @property string $amount
 * @property string $interest
 * @property int $duration
 * @property string $start_date
 * @property string $end_date
 * @property int $campaign
 * @property bool $status
 */
class Loan extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'interest', 'duration', 'start_date', 'end_date', 'campaign'], 'required'],
            [['user_id', 'duration', 'campaign'], 'integer'],
            [['amount', 'interest'], 'number'],
            [['status'], 'default', 'value' => 0],
            ['start_date', 'date', 'timestampAttribute' => 'start_date'],
            ['end_date', 'date', 'timestampAttribute' => 'end_date'],
            ['end_date', 'compare', 'compareAttribute' => 'start_date', 'operator' => '>', 'enableClientValidation' => true]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'amount' => 'Amount',
            'interest' => 'Interest',
            'duration' => 'Duration',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'campaign' => 'Campaign',
            'status' => 'Status',
        ];
    }

    /**
     * @return bool|void
     */
    public function afterValidate()
    {
        /**
         * Validate for legal age constraint
         */
        if ($this->user_id && User::findOne($this->user_id)->isUnderage()) {
            $this->addError('user_id', 'User is underage (-' . User::legalAge . ').');
            return false;
        }

        return true;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->start_date = date('Y-m-d', $this->start_date);
        $this->end_date = date('Y-m-d', $this->end_date);

        return parent::beforeSave($insert);
    }
}
