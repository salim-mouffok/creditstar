<?php

namespace app\models;

use app\services\UserService;
use yii\db\ActiveRecord;

/**
 * Class User
 *
 * @package app\models
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool $active
 * @property bool $dead
 * @property string $lang
 */
class User extends ActiveRecord
{
    const legalAge = 18;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['personal_code', 'phone'], 'integer'],
            [
                ['personal_code'],
                'integer',
                'min' => 10000000000,
                'max' => 69912319999,
                'tooBig' => 'Invalid personal code.',
                'tooSmall' => 'Invalid personal code.'
            ],
            [['active', 'dead'], 'boolean'],
            [['email'], 'email']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
        ];
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getAge()
    {
        return UserService::getAge($this->personal_code);
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return bool
     */
    public function isUnderage()
    {
        if ($this->age < self::legalAge) {
            return true;
        }

        return false;
    }
}
