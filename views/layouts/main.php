<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="top-head">
        <?php
        NavBar::begin([
            'options' => [
                'class' => 'navbar-inverse',
            ],
        ]);
        echo '<span class="navbar-text">Customer Service: 1715</span>';
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                Yii::$app->user->isGuest ? (
                    '<li>'
                    . Html::beginForm(['/site/login'], 'get')
                    . Html::submitButton(
                        'Login',
                        ['class' => 'btn btn-warning', 'style' => 'margin: 12px;']
                    )
                    . Html::endForm()
                    . '</li>'
                ) : (
                     '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Log out',
                        ['class' => 'btn btn-warning', 'style' => 'margin: 12px;']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);

        if (!Yii::$app->user->isGuest) {
            echo '<span class="navbar-right navbar-text">Welcome, ' . Yii::$app->user->identity->username . '</span>';
        }
        NavBar::end();
        ?>
    </div>
    <div class="container" style="margin-top: -36px; margin-bottom: -36px; border: 0">
        <img src="assets/creditstar-logo.png" alt="Creditstar Logo" />
    </div>
    <div>
        <?php
        if (!Yii::$app->user->isGuest) {
            NavBar::begin();
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => [
                    ['label' => 'Loans', 'url' => ['/loans/index']],
                    ['label' => 'Users', 'url' => ['/users/index']]
                ],
            ]);
            NavBar::end();
        }
        ?>
    </div>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">By Salim Mouffok</p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
