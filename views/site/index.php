<?php

/* @var $this yii\web\View */

$this->title = 'Homepage';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>
        <?php
        if (Yii::$app->user->isGuest) {
            echo '<p class="lead">Please login to proceed.</p>';
        }
        ?>
    </div>

</div>
