<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Loan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->dropDownList( \yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'fullName'),['prompt'=>'']) ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'interest')->textInput() ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'start_date')->widget(\yii\jui\DatePicker::className(), ['options'=> ['class'=>'form-control']]) ?>

    <?= $form->field($model, 'end_date')->widget(\yii\jui\DatePicker::className(), ['options'=> ['class'=>'form-control']]) ?>

    <?= $form->field($model, 'campaign')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
