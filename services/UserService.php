<?php

namespace app\services;

use yii\base\Exception;

/**
 * Class UserService
 * @package app\services
 */
class UserService
{
    /**
     * @param string $personalCode
     * @return int
     * @throws Exception
     */
    public static function getAge(string $personalCode)
    {
        $century = self::getCentury($personalCode);
        $yearOfBirth = $century . substr($personalCode, 1, 2);
        $monthOfBirth = substr($personalCode, 3, 2);
        $dayOfBirth = substr($personalCode, 5, 2);

        $dateOfBirth = $yearOfBirth . '/' . $monthOfBirth . '/' . $dayOfBirth;
        $age = \DateTime::createFromFormat('Y/m/d', $dateOfBirth)
            ->diff(new \DateTime('now'))
            ->y;

        return $age;
    }

    /**
     * @param string $personalCode
     * @return string
     * @throws Exception
     */
    private static function getCentury(string $personalCode)
    {
        switch ($personalCode[0]) {
            case 1:
            case 2:
                return '18';
                break;
            case 3:
            case 4:
                return '19';
                break;
            case 5:
            case 6:
                return '20';
                break;
            default:
                throw new Exception('Unrecognized century in personal code');
        }
    }
}