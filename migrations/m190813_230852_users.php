<?php

use yii\db\Migration;

/**
 * Class m190813_230852_users
 */
class m190813_230852_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $usersJson = file_get_contents('/app/users.json');
        $users = json_decode($usersJson);

        foreach ($users as $user) {
            $this->insert('user', $user);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user');
    }
}
