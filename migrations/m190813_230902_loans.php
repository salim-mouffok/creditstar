<?php

use yii\db\Migration;

/**
 * Class m190813_230902_loans
 */
class m190813_230902_loans extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $loansJson = file_get_contents('/app/loans.json');
        $loans = json_decode($loansJson);

        foreach ($loans as $loan) {
            $loan->start_date = date('Y-m-d', $loan->start_date);
            $loan->end_date = date('Y-m-d', $loan->end_date);
            $this->insert('loan', $loan);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('loan');
    }
}
